package JSONObjects;

import java.util.ArrayList;

public class JSONTesting {

	public static void main(String[] args) {
		JSONObject root = new JSONObject("root");
		System.out.println("root is " + root.objectID);
		JSONObject first = root.addObjectArrayChild("arraytest");
		System.out.println("first is"+first.objectID);
		JSONObject second = root.addStringChild("demo","real");
		System.out.println(second.objectID+ " "+first.objectID);
		JSONObject third = first.addObjectChild("testobj");
		System.out.println("third is "+third.objectID);
		JSONObject fourth = root.addStringArrayChild("numberfour");
		System.out.println(fourth.objectID);
		JSONObject fifth = third.addStringChild("test", "wow");
		JSONObject sixth = third.addStringChild("test2", "wow2");
		JSONObject seventh = first.addObjectChild("testobj2");
		JSONObject eighth = third.addStringChild("test3", "wow3");
		JSONObject ninth = third.addStringArrayChild("testing");
		ninth.addStringToArray("woop");
		ninth.addStringToArray("doop");
		JSONObject tenth = third.addObjectArrayChild("doh");
		JSONObject eleventh = tenth.addObjectChild("wowee");
		JSONObject twelfth = tenth.addObjectChild("wowzo");
		eleventh.addIntegerChild("numbery", 2);
		eleventh.addIntegerChild("numberys", 3);
		twelfth.addIntegerChild("numberoo", 2);
		seventh.addIntegerChild("number1", 1);
		System.out.println(fifth.objectID);
		fourth.addStringToArray("do");
		fourth.addStringToArray("do1");
		fourth.addStringToArray("do2");
		System.out.println("root "+root.getObjectID());
		System.out.println("first "+first.getObjectID() + " : " +first.getParentID());
		ArrayList<JSONObject> ordered = JSONObject.orderedObjects;
		System.out.println(ordered.size());
		System.out.println("{");
		for(int i = 0;i<ordered.size();i++) {
			JSONObject object = ordered.get(i);
			String tabs = " ";
			String comma ="";
			if(i<ordered.size()-1) {
				comma=",";
			}
			for(int j=0;j<object.depth-1;j++) {
				tabs+="\t";
			}
			System.out.println(tabs+object.printNameValue()+comma);
		}
		System.out.println("}");
		System.out.println("demo:\n\n"+root.getStringPropertiesForObject());
		
	}

}
