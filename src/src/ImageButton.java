package src;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class ImageButton extends JButton{
	private static final long serialVersionUID = -6027310079040478979L;
	private String imageFilePath;
	private int imageWidth;
	private int imageHeight;
	private int buttonWidth;
	private int buttonHeight;
	private int x;
	private int y;
	
	ImageButton(String filePath,int imageWidth,int imageHeight,int buttonWidth, int buttonHeight){
		this.setFocusPainted(false);
		this.setContentAreaFilled(false);
		this.imageWidth = imageWidth;
		this.imageHeight = imageHeight;
		this.imageFilePath = filePath;
		this.buttonWidth = buttonWidth;
		this.buttonHeight = buttonHeight;
		this.setHorizontalAlignment(SwingConstants.LEFT);
	    this.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		InputStream imgStream = JSONLister.class.getResourceAsStream(imageFilePath);
		BufferedImage buttonImage;
		//scales image and assigns to button
		try {
			buttonImage = ImageIO.read(imgStream);
			Image scaled = buttonImage.getScaledInstance(imageWidth, imageHeight, Image.SCALE_AREA_AVERAGING);
			ImageIcon icon = new ImageIcon(scaled);
			this.setIcon(icon);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ImageButton cloneButton(JSONLister j){
		ImageButton clone = new ImageButton(this.imageFilePath,this.imageWidth, this.imageHeight, this.buttonWidth, this.buttonHeight);
		clone.addActionListener(j);
		return clone;
	}
	
	public void setPosition(int newXPosition, int newYPosition) {
		this.x = newXPosition;
		this.y = newYPosition;
		this.setBounds(this.x, this.y, this.buttonWidth, this.buttonHeight);
	}
	public void setButtonDimensions(int newWidth,int newHeight) {
		this.buttonWidth = newWidth;
		this.buttonHeight = newHeight;
		this.setBounds(this.x, this.y, this.buttonWidth, this.buttonHeight);
	}
}
