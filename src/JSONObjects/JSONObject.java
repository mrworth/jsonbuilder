package JSONObjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Queue;

public class JSONObject {

		public static int objectCounter;
		public static HashMap<Integer,JSONObject> objectsByID = new HashMap<Integer,JSONObject>();
		public static ArrayList<JSONObject> orderedObjects = new ArrayList<JSONObject>();
		public int objectID;
		public int parentID;
		public int depth;
		
		public String name;
		
		//data type held (specified by JSONDataType)
		private String JSONDataType;
		private String jString="";
		
		private ArrayList<String> jStrings = new ArrayList<String>();
		private Integer jNumber;
		private ArrayList<Integer> jNumbers = new ArrayList<Integer>();
		private ArrayList<JSONObject> childObjects = new ArrayList<JSONObject>();
		
		
		JSONObject(String dataType){
			if(dataType.equals("root")) {
				objectCounter = 0;
				depth = 0;
				this.parentID = -1;
				this.JSONDataType = dataType;
				this.name = "root";
				this.objectID=JSONObject.objectCounter;
				objectsByID.put(objectCounter, this);
				//orderedObjects.add(this);
				objectCounter++;
			}else {
				this.JSONDataType = dataType;
				this.objectID=JSONObject.objectCounter;
				objectsByID.put(objectCounter, this);
				objectCounter++;
			}
		}
		
		public JSONObject addStringChild(String name,String value) {
			JSONObject newString = new JSONObject("String");
			newString.name = name;
			newString.jString = value;
			newString.parentID = this.objectID;
			System.out.println(this.name);
			System.out.println("IDS: "+objectID+"|"+newString.parentID+"|"+newString.objectID);
			newString.depth = this.depth+1;
			this.childObjects.add(newString);
			newString.insertBeforeNextSiblingOfParent();
			return newString;
		}
		public JSONObject addStringArrayChild(String name) {
			JSONObject newStringArray = new JSONObject("String[]");
			newStringArray.name = name;
			newStringArray.parentID = this.objectID;
			System.out.println("IDS: "+this.objectID+"|"+newStringArray.parentID+"|"+newStringArray.objectID);
			newStringArray.depth = this.depth+1;
			this.childObjects.add(newStringArray);
			newStringArray.insertBeforeNextSiblingOfParent();
			return newStringArray;
		}
		public JSONObject addIntegerChild(String name,Integer value) {
			JSONObject newInteger = new JSONObject("Integer");
			newInteger.name = name;
			newInteger.jNumber = value;
			newInteger.parentID = this.objectID;
			System.out.println("IDS: "+this.objectID+"|"+newInteger.parentID+"|"+newInteger.objectID);
			newInteger.depth = this.depth+1;
			this.childObjects.add(newInteger);
			newInteger.insertBeforeNextSiblingOfParent();
			return newInteger;
		}
		public JSONObject addIntegerArrayChild(String name) {
			JSONObject newIntegerArray = new JSONObject("Integer[]");
			newIntegerArray.name = name;
			newIntegerArray.parentID = this.objectID;
			newIntegerArray.depth = this.depth+1;
			this.childObjects.add(newIntegerArray);
//			orderedObjects.add(newIntegerArray);
			newIntegerArray.insertBeforeNextSiblingOfParent();
			return newIntegerArray;
		}
		public JSONObject addObjectChild(String name) {
			JSONObject newObject = new JSONObject("Object");
			newObject.name = name;
			newObject.parentID = this.objectID;
			newObject.depth = this.depth+1;
			this.childObjects.add(newObject);
			newObject.insertBeforeNextSiblingOfParent();
			return newObject;
		}
		public JSONObject addObjectArrayChild(String name) {
			JSONObject newObjectArray = new JSONObject("Object[]");
			newObjectArray.name = name;
			newObjectArray.parentID = this.objectID;
			newObjectArray.depth = this.depth+1;
			this.childObjects.add(newObjectArray);
			newObjectArray.insertBeforeNextSiblingOfParent();
			return newObjectArray;
		}
		public void addIntegerToArray(Integer value) {
			this.jNumbers.add(value);
		}
		public void addStringToArray(String value) {
			this.jStrings.add(value);
		}
		public int getObjectID() {
			return this.objectID;
		}
		public int getParentID() {
			return this.parentID;
		}
		public void insertBeforeNextSiblingOfParent() {
			int parentId = this.getParentID();
			System.out.println("parent id is "+parentId);
			System.out.println("object id is "+this.objectID);
			JSONObject parentObject = objectsByID.get(parentId);
			//root's Id is 0 so there is no grandparent
			if(parentId != 0) {
				
				int grandparentId = parentObject.getParentID();
				JSONObject grandparentObject = objectsByID.get(grandparentId);
				int indexOfParent = grandparentObject.childObjects.indexOf(parentObject);
				//adds to the location of the next sibling of the parent if the next sibling exists, otherwise adds to the end of ordered objects
				if(indexOfParent<grandparentObject.childObjects.size()-1) {
					int nextSiblingId = grandparentObject.childObjects.get(indexOfParent+1).objectID;
					JSONObject nextSiblingObject = objectsByID.get(nextSiblingId);
					int indexOfNextSiblingObject = orderedObjects.indexOf(nextSiblingObject);
					System.out.println("next sibling : "+indexOfNextSiblingObject);
					System.out.println("parent : "+indexOfParent);
					orderedObjects.add(indexOfNextSiblingObject,this);
				}else {
					orderedObjects.add(this);
				}
			}else {
				orderedObjects.add(this);
			}
		}
		public String printNameValue() {
			String nameString="";
			JSONObject parentObject = objectsByID.get(this.parentID);
			if(!parentObject.JSONDataType.equals("Object[]")) {
				nameString = "\"" + name + "\" : ";
			}
			
			switch(JSONDataType) {
			case "String": 
					return nameString + "\"" + this.jString+"\"";
			case "String[]":
				String values = "[";
				for(int i=0;i<this.jStrings.size();i++) {
					if(i<jStrings.size()-1) {
						values += "\""+jStrings.get(i)+"\",";
					}else {
						values += "\""+jStrings.get(i)+"\"]";
					}
				}
				return nameString  + values;
			case "Integer":
				return nameString + this.jNumber+"";
			case "Integer[]": 
				String numbers = "[";
				for(int i=0;i<this.jNumbers.size();i++) {
					if(i<jNumbers.size()-1) {
						numbers += ""+jNumbers.get(i)+",";
					}else {
						numbers += ""+jNumbers.get(i)+"]";
					}
				}
				return nameString + numbers;
			case "Object":
				return getStringPropertiesForObject();
			case "Object[]":
				return getStringObjectsForObjectArray();
			default:
					return "undefined";
			}
		}
		public static String makeTabs(int depth) {
			String tabs = "";
			for(int i=0;i<depth;i++) {
				tabs = tabs +"\t";
			}
			return tabs;
		}
		public String getTabs() {
			return makeTabs(this.depth);
		}
		public boolean isLastChildOfParent(JSONObject object) {
			if(objectID==0) {
				return false;
			}
			JSONObject parentObject = objectsByID.get(object.parentID);
			ArrayList<JSONObject> siblings = parentObject.childObjects;
			int indexOfThisObject = siblings.indexOf(object);
			if(indexOfThisObject<siblings.size()-1) {
				return false;
			}else {
				return true;
			}
		}
		public String getStringPropertiesForObject() {
			String lastItemComma = "";
			if(this.objectID !=0 && !isLastChildOfParent(this)) {
				lastItemComma = ",";
			}

			JSONObject parentObject = objectsByID.get(this.parentID);
			String objectString="";
			if(objectID!=0 && !parentObject.JSONDataType.equals("Object[]")) {
				objectString = " "+this.getTabs()+"\""+name+ "\": {";
			}else {
				objectString = " "+this.getTabs()+"{";
			}
			
			
			for(int i=0;i<this.childObjects.size();i++) {
				JSONObject object = this.childObjects.get(i);
				objectString+= "\n "+object.getTabs()+object.printNameValue();
				if(i<childObjects.size()-1) {
					objectString+=",";
				}
			}
			objectString+="\n "+this.getTabs()+"}"+lastItemComma;
			return objectString;
		}
		public String getStringObjectsForObjectArray() {
			String objectString = ""+"\""+name+ "\": [";
			
			for(JSONObject object : this.childObjects) {
				
				objectString+= "\n "+object.getStringPropertiesForObject();
			}
			objectString+="\n "+this.getTabs()+"]";
			return objectString;
		}
}